/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab02;

/**
 *
 * @author pasinee
 */
public class ArrayDeletionsLab {

    public static void main(String[] args) {
        // Test cases
        int[] arr1 = {1, 2, 3, 4, 5};
        int index1 = 2;
        System.out.println("Original Array: " + arrayToString(arr1));
        int[] updatedArray1 = deleteElementByIndex(arr1, index1);
        System.out.println("Array after deleting element at index " + index1 + ": " + arrayToString(updatedArray1));

        int[] arr2 = {1, 2, 3, 4, 5};
        int value1 = 4;
        System.out.println("Original Array: " + arrayToString(arr2));
        int[] updatedArray2 = deleteElementByValue(arr2, value1);
        System.out.println("Array after deleting element with value " + value1 + ": " + arrayToString(updatedArray2));
    }

    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            // Handle invalid index, return the original array
            return arr;
        }

        int[] updatedArray = new int[arr.length - 1];
        int currentIndex = 0;

        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                updatedArray[currentIndex] = arr[i];
                currentIndex++;
            }
        }

        return updatedArray;
    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int indexToDelete = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                indexToDelete = i;
                break;
            }
        }

        if (indexToDelete == -1) {
            // Value not found, return the original array
            return arr;
        }

        return deleteElementByIndex(arr, indexToDelete);
    }

    public static String arrayToString(int[] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if (i < arr.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
